imu_data = table2array(imudata);
figure;
plot(imu_time,imu_data(1:70171, 1))
hold on
plot(imu_time,imu_data(1:70171, 2))
plot(imu_time,imu_data(1:70171, 3))
legend({'Yaw', 'Pitch', 'Roll'})
hold off
raw_yaw = imu_data(:, 1);
raw_pitch = imu_data(:, 2);
raw_rool = imu_data(:, 3);
mag_x = imu_data(:, 4);
mag_y = imu_data(:, 5);
mag_z = imu_data(:, 6);
accel_x = imu_data(:, 7);
accel_y = imu_data(:, 8);
accel_z = imu_data(:, 9);
gyro_x = imu_data(:, 10);
gyro_y = imu_data(:, 11);
gyro_z = imu_data(:, 12);

figure;
plot(imu_time(900:7000), mag_x(900:7000))
figure;
plot(imu_time(1:6800), mag_y(1:6800))

mag_x_cen = mag_x - mean(mag_x(900:7000));
mag_y_cen = mag_y - mean(mag_y(1:6800));
% Solving hard iron issues
plot(imu_time(7500:11500), mag_x_cen(7500:11500))
plot(mag_x_cen(7500:11500), mag_y_cen(7500:11500))
hard_iron_x = (max(mag_x_cen(7500:11500)) + min(mag_x_cen(7500:11500)))./2;
hard_iron_y = (max(mag_y_cen(7500:11500)) + min(mag_y_cen(7500:11500)))./2;
mag_x_hi_comp = mag_x_cen - hard_iron_x;
mag_y_hi_comp = mag_y_cen - hard_iron_y;
plot(mag_x_hi_comp(7500:11500), mag_y_hi_comp(7500:11500))
% Hard iron neutralized
% Solving Soft Iron issues
for c= 7500:11500
magn_for_axis(c-7499) = sqrt(mag_x_hi_comp(c)^2 + mag_y_hi_comp(c)^2);
end
[r, i] = max(magn_for_axis)
q = min(magn_for_axis);

R = [[cos(theta) sin(theta)]; [-sin(theta) cos(theta)]]
R_inv = [[cos(-theta) sin(-theta)]; [-sin(-theta) cos(-theta)]]
mag_xy_vector = [mag_x_hi_comp mag_y_hi_comp];
rot_mag_xy_vector = mag_xy_vector*R;
scaling_factor = q/r;
rot_mag_xy_vector(:, 1) = rot_mag_xy_vector(:, 1)./scaling_factor;
rev_rot_mag_xy_vector = rot_mag_xy_vector*R_inv;
plot(mag_x_hi_comp(7500:11500), mag_y_hi_comp(7500:11500))
figure;
plot(rev_rot_mag_xy_vector(7500:11500, 1), rev_rot_mag_xy_vector(7500:11500, 2))
mag_yaw = atan2(-rev_rot_mag_xy_vector(:, 2), rev_rot_mag_xy_vector(:, 1));
plot(imu_time, rad2deg(mag_yaw(1:70171)))
figure;
plot(imu_time,imu_data(1:70171, 1))

