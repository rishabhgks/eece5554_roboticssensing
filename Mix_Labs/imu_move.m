move_gps = table2array(movegps);
figure;
plot(move_gps(:, 2), move_gps(:, 3));
title('Easting vs Northing for the moving data');
xlabel('Easting');
ylabel('Northing');



% 
% mag_test_y = robot_mag.Magnetic.magnetic_field_y(6975:7850);
% plot(mag_test_x + 0.015, mag_test_y - 0.2)
% plot(smoothdata(mag_test_x, 'gaussian') + 0.015, smoothdata(mag_test_y, 'gaussian') - 0.2)
% plot(smoothdata(robot_mag.Magnetic.magnetic_field_x, 'gaussian'), smoothdata(robot_mag.Magnetic.magnetic_field_y, 'gaussian'))

for c = 1:length(robot_mag.Magnetic.magnetic_field_x)-40
mag_t_x(c:c+40) = smoothdata(robot_mag.Magnetic.magnetic_field_x(c:c+40), 'gaussian', 60);
mag_t_y(c:c+40) = smoothdata(robot_mag.Magnetic.magnetic_field_y(c:c+40), 'gaussian', 60);
c = c+40;
end

alpha = (max(mag_t_x(6975:7850)) + min(mag_t_x(6975:7850)))/2;
beta = (max(mag_t_y(6975:7850)) + min(mag_t_y(6975:7850)))/2;

mag_t_x = mag_t_x - alpha;
mag_t_y = mag_t_y - beta;

for c = 1:7850-6975
magn_for_axis(c) = sqrt(mag_t_x(c+6974)^2 + mag_t_y(c+6974)^2);
end

[r, i] = max(magn_for_axis);
[q, i2] = min(magn_for_axis);
theta = asin(mag_t_y(i+6974)/r)
R = [[cos(theta) sin(theta)]; [-sin(theta) cos(theta)]]
R_inv = [[cos(-theta) sin(-theta)]; [-sin(-theta) cos(-theta)]]

% conv_mag_t_x = mag_t_x*R(1, 1) + mag_t_y*R(2, 1);
% conv_mag_t_y = mag_t_x*R(1, 2) + mag_t_y*R(2, 2);
[conv_mag_t_x conv_mag_t_y] = [mag_t_x mag_t_y]*R;

scaling_factor = q/r;
scal_conv_mag_t_x = conv_mag_t_x/scaling_factor;
% rev_rot_scal_conv_mag_t_x = scal_conv_mag_t_x*R_inv(1, 1) + conv_mag_t_y*R_inv(2, 1);
% rev_rot_conv_mag_t_y = scal_conv_mag_t_x*R_inv(1, 2) + conv_mag_t_y*R_inv(2, 2);
[rev_rot_scal_conv_mag_t_x rev_rot_conv_mag_t_y] = [scal_conv_mag_t_x conv_mag_t_y]*R_inv;

plot(rev_rot_scal_conv_mag_t_x(6975:7850), rev_rot_conv_mag_t_y(6975:7850))
figure;
plot(mag_t_x(6975:7850), mag_t_y(6975:7850))

plot(rev_rot_scal_conv_mag_t_x(5900:9400), rev_rot_conv_mag_t_y(5900:9400))
figure;
plot(mag_t_x(5900:9400), mag_t_y(5900:9400))

for c = 1:length(robot_imu.Imu.orientation_quat_x)
rpy(c, :) = quat2eul([robot_imu.Imu.orientation_quat_w(c) robot_imu.Imu.orientation_quat_x(c) robot_imu.Imu.orientation_quat_y(c) robot_imu.Imu.orientation_quat_z(c)]);
end

for c=1: length(rev_rot_scal_conv_mag_t_x)
yaw_mag(c) = atand(-rev_rot_conv_mag_t_y(c)/rev_rot_scal_conv_mag_t_x(c));
end
yaw_mag = reshape(yaw_mag, length(yaw_mag), 1);

imu_time = robot_imu.Imu.msg_time;
imu_time = imu_time - 1.581114238446251e+09;
gyro_yaw = cumtrapz(robot_imu.Imu.angular_velocity_z, imu_time);
mag_time = robot_mag.Magnetic.msg_time;
mag_time = mag_time - 1.581114238478413e+09;
plot(imu_time, gyro_yaw)
hold on
plot(mag_time, yaw_mag)
legend({'gyro', 'magno'})
hold off

plot(imu_time, highpass(gyro_yaw, 0.8))
hold on
plot(mag_time, lowpass(yaw_mag, 0.2))
legend({'gyro', 'magno'})
hold off

complimentary_yaw = 0.98*highpass(gyro_yaw(1:70163), 0.8) + 0.02*lowpass(yaw_mag, 0.2);
plot(mag_time, wrapToPi(complimentary_yaw))
hold on
plot(imu_time, rpy(:, 3))
legend({'Complimentary Filtered Fusion Data', 'IMU Raw Yaw'})
hold off
complimentary_yaw = 0.98*highpass(gyro_yaw(1:70163), 0.8) + 0.02*lowpass(yaw_mag, 0.2);
plot(mag_time, (complimentary_yaw))
hold on
plot(imu_time, unwrap(rpy(:, 3)))
legend({'Complimentary Filtered Fusion Data', 'IMU Raw Yaw'})
hold off


% lat = move_gps(:, 2);
% lon = move_gps(:, 3);
% dLat = diff(lat);
% dLon = diff(lon);
% dT = diff(move_gps(:, 1));
% distLatLon = hypot(dLat, dLon);
% distLatLonMet = distLatLon*4E+7/360;
% gps_vel = distLatLonMet./dT;

% accelo_forward_velo = cumtrapz(robot_imu.Imu.acceleration_x, diff(robot_imu.Imu.msg_time));
accelo_forward_velo = cumtrapz(imu_time, robot_imu.Imu.acceleration_x);
% plot(imu_time, accelo_forward_velo)
% hold on
% plot(dT, gps_vel)
% hold off

plot(move_gps(1:1753, 1), gps_vel)
abs(accelo_forward_velo)
clc
plot(imu_time, abs(accelo_forward_velo))
figure;
plot(move_gps(1:1753, 1), gps_vel)
utmDiff = diff(move_gps(:, 4:5))
utmVel = utmDiff./dT;
plot(move_gps(1:1753, 1), utmVel(:, 1))
figure;
plot(imu_time, accelo_forward_velo)

corrected_accel_x = robot_imu.Imu.acceleration_x - ((mean(robot_imu.Imu.acceleration_x) - mean(table2array(stat_accel_x_0)))/2);
corrected_accelo_forward_velo = cumtrapz(corrected_accel_x, imu_time);
plot(move_gps(1:1753, 1), utmVel(:, 1))
figure;
plot(imu_time, smoothdata(corrected_accelo_forward_velo, 'gaussian'))
corrected_accel_x = robot_imu.Imu.acceleration_x - ((mean(robot_imu.Imu.acceleration_x) - mean(table2array(stat_accel_x_0)))/2);
corrected_accelo_forward_velo = cumtrapz(corrected_accel_x, imu_time);
plot(move_gps(1:1753, 1), utmVel(:, 1))
figure;
plot(imu_time, smoothdata(corrected_accelo_forward_velo, 'gaussian'))
figure;
plot(imu_time, smoothdata(accelo_forward_velo, 'gaussian'))

gyro_yaw_test = cumtrapz(imu_time(10000:66000), robot_imu.Imu.angular_velocity_z(10000:66000));
mag_yaw_test = atan2(rev_rot_scal_conv_mag_t_x(10000:66000), rev_rot_conv_mag_t_y(10000:66000));
mag_yaw_test = reshape(mag_yaw_test, length(mag_yaw_test), 1);
plot(imu_time(10000:66000), wrapToPi(cumtrapz(imu_time(10000:66000), robot_imu.Imu.angular_velocity_z(10000:66000))))
hold on
plot(mag_time(10000:66000), wrapToPi(atan2(rev_rot_scal_conv_mag_t_x(10000:66000), rev_rot_conv_mag_t_y(10000:66000))))
hold off
figure;
plot(imu_time(10000:66000), 0.98*(wrapToPi(gyro_yaw_test)) + 0.02*(wrapToPi(mag_yaw_test)))

complimentary_yaw_test = 0.98*(wrapToPi(gyro_yaw_test)) + 0.02*(wrapToPi(mag_yaw_test));


complimentary_yaw_test = 0.98*(wrapToPi(gyro_yaw_test)) + 0.02*(wrapToPi(mag_yaw_test));
plot(imu_time(10000:66000), wrapToPi(cumtrapz(imu_time(10000:66000), robot_imu.Imu.angular_velocity_z(10000:66000))))
hold on
plot(mag_time(10000:66000), wrapToPi(atan2(rev_rot_scal_conv_mag_t_x(10000:66000), rev_rot_conv_mag_t_y(10000:66000))))
hold off
figure;
plot(imu_time(10000:66000), complimentary_yaw_test)
complimentary_yaw_test = 0.92*(wrapToPi(gyro_yaw_test)) + 0.08*(wrapToPi(mag_yaw_test));
plot(imu_time(10000:66000), wrapToPi(cumtrapz(imu_time(10000:66000), robot_imu.Imu.angular_velocity_z(10000:66000))))
hold on
plot(mag_time(10000:66000), wrapToPi(atan2(rev_rot_scal_conv_mag_t_x(10000:66000), rev_rot_conv_mag_t_y(10000:66000))))
hold off
figure;
plot(imu_time(10000:66000), complimentary_yaw_test)
plot(imu_time(10000:66000), wrapToPi(cumtrapz(imu_time(10000:66000), robot_imu.Imu.angular_velocity_z(10000:66000))))
hold on
plot(mag_time(10000:66000), wrapToPi(atan2(rev_rot_scal_conv_mag_t_x(10000:66000), rev_rot_conv_mag_t_y(10000:66000))))
hold off
figure;
plot(imu_time(10000:66000), complimentary_yaw_test)
figure;
plot(imu_time(10000:66000), rpy(10000:66000,3))