stat2_data = table2array(stat2);
format long;
unique(stat2_data(:, 2))
unique(stat2_data(:, 3))
scatter(stat2_data(:, 2), stat2_data(:, 3))
scatter(stat2_data(:, 1), stat2_data(:, 2))
scatter(stat2_data(:, 1), stat2_data(:, 3))
histogram(stat2_data(:, 2))
histogram(stat2_data(:, 3))
scatter(stat2_data(:, 2), stat2_data(:, 3))
hold on
scatter([327986], [4689521])
hold off
std(stat2_data(:, 2))
std(stat2_data(:, 3))
hist(stat2_data(:, 2) - mean(stat2_data(:, 2)), 15)
hist(stat2_data(:, 3) - mean(stat2_data(:, 3)), 15)

length(unique(move2_data(:, 2)))
length(unique(move2_data(:, 3)))
scatter(move2_data(:, 2), move2_data(:, 3))
scatter(move2_data(:, 1), move2_data(:, 2))
scatter(move2_data(:, 1), move2_data(:, 3))
scatter(move2_data(:, 2), move2_data(:, 3))
hold on
scatter([327683], [4689541])
hold off
p = polyfit(move2_data(:, 2), move2_data(:, 3), 1)
poly = polyval(p, move2_data(:, 2))
scatter(move2_data(:, 2), move2_data(:, 3))
hold on
scatter(move2_data(:, 2), poly)
hold off
hist(move2_data(:, 3) - poly, 50)