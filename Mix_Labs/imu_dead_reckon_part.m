imu = table2array(imu_data);
mag = table2array(mag_data);
gps = table2array(gps_data);

orig_time = imu(:, 1)./10^9;

imu_linear_acc_x = imu(:, 9);
imu_linear_acc_y = imu(:, 10);
imu_linear_acc_z = imu(:, 11);

imu_angu_vel_x = imu(:, 6);
imu_angu_vel_y = imu(:, 7);
imu_angu_vel_z = imu(:, 8);

gyro_yaw = wrapToPi(cumtrapz(orig_time, imu(:, 8)));

imu_true_yaw = rpy_arr(:, 1);

mean_fwd_vel = mean(imu_linear_acc_x(1:4600));
x_obs_dot_dot = imu_linear_acc_x - mean_fwd_vel;

X_dot = cumtrapz(orig_time, x_obs_dot_dot);
omega = imu_angu_vel_z;

y_obs_dot_dot = imu_linear_acc_y;

% plot(orig_time, omega.*X_dot);
% hold on
% plot(orig_time, y_obs_dot_dot);
% xlabel('Time')
% ylabel('Acceleration')
% title('Y Observed vs Omega X_Dot')
% legend({'omega-X-dot', 'y-obs-dot-dot'})
% hold off
% 
% omega = highpass(imu_angu_vel_z, 0.0009);
% figure;
% plot(orig_time, omega.*X_dot);
% hold on
% plot(orig_time, y_obs_dot_dot);
% xlabel('Time')
% ylabel('Acceleration')
% title('Y Observed vs Omega X_Dot (cleaned by highpass)')
% legend({'omega-X-dot', 'y-obs-dot-dot'})
% hold off

% x_dot_dot = x_obs_dot_dot + (omega.*X_dot);
x_dot_dot = x_obs_dot_dot + y_obs_dot_dot;
v = cumtrapz(orig_time, x_dot_dot);
ve = v.*cos(deg2rad(imu_true_yaw));
vn = v.*sin(deg2rad(imu_true_yaw));
xe = cumtrapz(orig_time, ve)+850;
xn = cumtrapz(orig_time, vn)+1900;

offset_theta = 1.1050;
R_gps = [cos(offset_theta) sin(offset_theta); -sin(offset_theta) cos(offset_theta)];
gps(:, 2) = gps(:, 2) - gps(1, 2);
gps(:, 3) = gps(:, 3) - gps(1, 3);
gps_rot = [gps(:, 2) gps(:, 3)]*R_gps;

figure;
plot(xe./23, -xn./23);
hold on
plot(gps_rot(:, 1), gps_rot(:, 2))
xlabel('X coordinate')
ylabel('Y coordinate')
title('IMU vs GPS Trajectory')
legend({'imu', 'gps'})
hold off