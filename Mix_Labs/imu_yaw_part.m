imu = table2array(imu_data);
mag = table2array(mag_data);
gps = table2array(gps_data);

% for c = 1:length(robot_mag.Magnetic.magnetic_field_x)-40
% mag_t_x(c:c+40) = smoothdata(robot_mag.Magnetic.magnetic_field_x(c:c+40), 'gaussian', 60);
% mag_t_y(c:c+40) = smoothdata(robot_mag.Magnetic.magnetic_field_y(c:c+40), 'gaussian', 60);
% c = c+40;
% end

% plot(imu(:, 1), rpy_arr(:, 3))
% hold on
% plot(imu(:, 1), rpy_arr(:, 2))
% plot(imu(:, 1), rpy_arr(:, 1))
% legend({'Roll', 'Pitch', 'Yaw'})
% hold off
% xlabel('Time')
% ylabel('Angle (in degrees)')
% title('Roll-Pitch-Yaw from the IMU Sensor')

% figure;
% plot(mag(:, 2), mag(:, 3))
% xlabel('Magnetometer X Axis data')
% ylabel('Magnetometer Y Axis data')
% title('Magnetometer X-Y plot before hard and soft iron corrections')

orig_time = imu(:, 1)./10^9;
time = (imu(:, 1) - imu(1, 1))./10^9;

% figure;
% % plot(time(4500:12500), mag(4500:12500, 2))
% plot(mag(8000:12500, 2), mag(8000:12500, 3))
% xlabel('Magnetometer X Axis data')
% ylabel('Magnetometer Y Axis data')
% title('Magnetometer X-Y plot before hard and soft iron corrections')


alpha = (max(mag(8000:12500, 2)) + min(mag(8000:12500, 2)))/2;
beta = (max(mag(8000:12500, 3)) + min(mag(8000:12500, 3)))/2;

mag_x = mag(:, 2) - alpha;
mag_y = mag(:, 3) - beta;

for c = 8000:12500
magn_for_axis(c-7999) = sqrt(mag_x(c)^2 + mag_y(c)^2);
end

[r, i] = max(magn_for_axis);
[q, i2] = min(magn_for_axis);
theta = asin(mag_y(i+7999)/r)
R = [[cos(theta) sin(theta)]; [-sin(theta) cos(theta)]]
R_inv = [[cos(-theta) sin(-theta)]; [-sin(-theta) cos(-theta)]]

% conv_mag_t_x = mag_t_x*R(1, 1) + mag_t_y*R(2, 1);
% conv_mag_t_y = mag_t_x*R(1, 2) + mag_t_y*R(2, 2);
conv_mag_vector = [reshape(mag_x, length(mag_x), 1) reshape(mag_y, length(mag_y), 1)]*R;


scaling_factor = q/r;
conv_mag_vector(:, 1) = conv_mag_vector(:, 1)./scaling_factor;
% rev_rot_scal_conv_mag_t_x = scal_conv_mag_t_x*R_inv(1, 1) + conv_mag_t_y*R_inv(2, 1);
% rev_rot_conv_mag_t_y = scal_conv_mag_t_x*R_inv(1, 2) + conv_mag_t_y*R_inv(2, 2);
rev_conv_mag_vector = conv_mag_vector*R_inv;
rev_rot_scal_conv_mag_t_x = rev_conv_mag_vector(:, 1);
rev_rot_conv_mag_t_y = rev_conv_mag_vector(:, 2);

% figure;
% plot(rev_rot_scal_conv_mag_t_x(8000:12500), rev_rot_conv_mag_t_y(8000:12500))
% figure;
% plot(mag_x(8000:12500), mag_y(8000:12500))
% xlabel('Magnetometer X Axis data')
% ylabel('Magnetometer Y Axis data')
% title('Magnetometer X-Y plot after hard and soft iron corrections')

% figure;
% plot(mag(:, 2), mag(:, 3))
% xlabel('Magnetometer X Axis data')
% ylabel('Magnetometer Y Axis data')
% title('Magnetometer X-Y plot after hard and soft iron corrections')

for c=1: length(rev_rot_scal_conv_mag_t_x)
yaw_mag(c) = atan2(-rev_rot_conv_mag_t_y(c),rev_rot_scal_conv_mag_t_x(c));
end
yaw_mag = reshape(yaw_mag, length(yaw_mag), 1);

figure;
plot(time, yaw_mag)

gyro_yaw = wrapToPi(cumtrapz(orig_time, imu(:, 8)));
figure
plot(orig_time, gyro_yaw);
hold on
plot(orig_time, yaw_mag)
xlabel('Time')
ylabel('Angle (in radians)')
title('Yaw angle from Magnetometer and Gyroscope (after integration)')
legend({'gyro', 'magno'})
hold off
% figure;
% plot(orig_time, highpass(gyro_yaw, 0.001))
% hold on
% plot(orig_time, lowpass(yaw_mag, 0.2))
% legend({'gyro', 'magno'})
% hold off

complimentary_yaw = 0.9*(gyro_yaw) + 0.1*(yaw_mag);
figure;
plot(orig_time, (rad2deg(complimentary_yaw)))
hold on
plot(orig_time, (rpy_arr(:, 1)))
xlabel('Time')
ylabel('Angle (in degrees)')
title('Complimentary Filter vs IMU Yaw')
legend({'Complimentary Filtered Fusion Data', 'IMU Raw Yaw'})
hold off

figure
plot(orig_time, unwrap(rad2deg(complimentary_yaw)))
hold on
plot(orig_time, unwrap(rpy_arr(:, 1)))
xlabel('Time')
ylabel('Angle (in degrees)')
title('Complimentary Filter vs IMU Yaw - Unwrapped')
legend({'Complimentary Filtered Fusion Data', 'IMU Raw Yaw'})
hold off