close all; clear; clc;

numbFrames = 100;
skipFrames = 10;
downsampleSize = 0.1;
regisSize = 5;
mergeSize = 0.5;

% This project used the helperLidarMapBuilder.m and it's associated matlab
% assets to help build the LiDAR dense map which is being used to
% desmostrate the point cloud registration technique used in this exercise.
% Download the lidar data set from following URL and use bagselect to load
% https://drive.google.com/open?id=1MiPApz0QMdPGunLu2G9QBLTkUDTqGvDt
bagselect = rosbag('/Volumes/Free_Space/LIDAR_Mapping_bicycle/2017-10-18-17-33-13_0.bag');
load('./FPDataset/bagselect.mat');
pointcloud = select(bagselect,'Topic','/ns1/velodyne_points');
%xyz = readMessages(pointcloud,'DataFormat','struct');

% Load lidar data from measurement data
% Due to it's size which prevent me from upload this to gitlab; I have
% provide the downloadable link here
% https://xritepantone.box.com/s/x2lwbndc2h0o92q1b6i0uajzz3shaxzy
temp = load(fullfile('/Volumes','Free_Space','FPDataSet','measurementLiDAR.mat'));
measurementLiDAR = temp.lidarPointClouds;

% Load GPS sequence from measurement data
temp = load(fullfile('/Volumes','Free_Space','FPDataSet','measurementGPS.mat'));
measurementGPS = temp.gpsSequence;

% Load IMU measurement from measurement data
temp = load(fullfile('/Volumes','Free_Space','FPDataSet','measurementIMU.mat'));
measurementIMU = temp.imuOrientations;

% Convert IMU measurement to quaternion
measurementIMU = convertvars(measurementIMU, 'Orientation', 'quaternion');

% Compute the approximate frame
lidarFrameDuration = median(diff(measurementLiDAR.Time));
gpsFrameDuration   = median(diff(measurementGPS.Time));
imuFrameDuration   = median(diff(measurementIMU.Time));

% Adjust display format to seconds
lidarFrameDuration.Format = 's';
gpsFrameDuration.Format   = 's';
imuFrameDuration.Format   = 's';

% Compute frame rates
lidarRate = 1/seconds(lidarFrameDuration);
gpsRate   = 1/seconds(gpsFrameDuration);
imuRate   = 1/seconds(imuFrameDuration);

% Visualize driving eata with a pcplayer to show streaming point clouds
% from lidar sensor
lidarPlayer = pcplayer([-50 50], [-50 50], [-15 25]);

% Customize player axes labels
xlabel(lidarPlayer.Axes, 'X (m)')
ylabel(lidarPlayer.Axes, 'Y (m)')
zlabel(lidarPlayer.Axes, 'Z (m)')

title(lidarPlayer.Axes, 'EECE5554 FP: Lidar Sensor Data')

for g = 1 : height(measurementGPS)-1
       
    % Compute the time span between the current and next GPS reading
    timeSpan = timerange(measurementGPS.Time(g), measurementGPS.Time(g+1));
    
    % Extract the lidar frames recorded during this time span
    lidarFrames = measurementLiDAR(timeSpan, :);
    
    % Inner loop over lidar readings (faster signal)
    for l = 1 : height(lidarFrames)
        
        % Extract point cloud data
        ptCloud = lidarFrames.PointCloud(l);
        
        % Update lidarPlayer
        view(lidarPlayer, ptCloud);
        
        % Pause to slow down the display
        pause(0.5)
    end
end

hide(lidarPlayer)


% Sample the point cloude data for ready registration
fixed  = measurementLiDAR.PointCloud(numbFrames);
moving = measurementLiDAR.PointCloud(numbFrames + skipFrames);


% Prior to registration, process the point cloud so as to retain structures
% in the point cloud that are distinctive. 
isOrganized = ~ismatrix(fixed.Location);

% If the point cloud is organized, use th range-based flood fill algorithm.
% or use plane fitting algorithm.
groundSegmentationMethods = ["planefit", "rangefloodfill"];
method = groundSegmentationMethods(isOrganized+1);

if method == "planefit"
    % Segment ground using planefit algorithm.
    [~,groundIdx] = pcfitplane(fixed, 0.4, [0, 0, 1], 5);
elseif method == "rangefloodfill"
    % Segment ground using range-based flood fill algorithm.
    groundIdx = segmentGroundFromLidarData(fixed);
end

egocarIdx  = findNeighborsInRadius(fixed, [0, 0, 0], 3.5);

% Remove points blong to the ground or vehicle itself
ptsToKeep = true(fixed.Count, 1);
ptsToKeep(groundIdx) = false;
ptsToKeep(egocarIdx) = false;

% If the point cloud is organized, retain organized structure
if isOrganized
    fixedProcessed = select(fixed, find(ptsToKeep), 'OutputSize', 'full');
else
    fixedProcessed = select(fixed, find(ptsToKeep));
end

% Repeat the process for points blong to moving
isOrganized = ~ismatrix(moving.Location);

% If the point cloud is organized, use th range-based flood fill algorithm.
% or use plane fitting algorithm.
groundSegmentationMethods = ["planefit", "rangefloodfill"];
method = groundSegmentationMethods(isOrganized+1);

if method == "planefit"
    % Segment ground using plane fitting algorithm.
    [~,groundIdx] = pcfitplane(moving, 0.4, [0, 0, 1], 5);
elseif method == "rangefloodfill"
    % Segment ground using range-based flood fill algorithm.
    groundIdx = segmentGroundFromLidarData(moving);
end

% Segment ego vehicle as points within a given radius of sensor
egocarIdx  = findNeighborsInRadius(moving, [0, 0, 0], 3.5);

% Remove points from the ground or the ego vehicle
ptsToKeep = true(moving.Count, 1);
ptsToKeep(groundIdx) = false;
ptsToKeep(egocarIdx) = false;

% If the point cloud is organized, retain organized structure
if isOrganized
    movingProcessed = select(moving, find(ptsToKeep), 'OutputSize', 'full');
else
    movingProcessed = select(moving, find(ptsToKeep));
end


% Display the raw and processed point clouds
displayFixed = figure;
pcshowpair(fixed, fixedProcessed)
% Create 2D view
view(2);
displayFixed.HandleVisibility = 'callback';


% Downsampling the point clouds prior to registration. 
fixedDownsampled  = pcdownsample(fixedProcessed, 'random', downsampleSize);
movingDownsampled = pcdownsample(movingProcessed, 'random', downsampleSize);


% After preprocessing the point clouds, Compare the registration methom
% with time and errors. 
tic;
% Register two point clouds using NDT algorithm
[tform,ndt_mR,ndt_rmse] = pcregisterndt(movingDownsampled, ...
    fixedDownsampled, regisSize);
st = toc;
fprintf('Total time for registration using NDT: %s seconds\n', st);
fprintf('The Root Mean Square ERROR using NDT: %2.4f pixel\n', ndt_rmse);


tic;
% Register two point clouds using ICP algorithm
[~,icp_mR,icp_rmse] = pcregistericp(movingDownsampled, fixedDownsampled);
st = toc;
fprintf('Total time for registration using ICP: %s seconds\n', st);
fprintf('The Root Mean Square ERROR using ICP: %2.4f pixel\n', icp_rmse);


tic;
% Register two point clouds using CPD algorithm
[~,cpd_mR,cpd_rmse] = pcregistercpd(movingDownsampled, fixedDownsampled);
st = toc;
fprintf('Total time for registration using CPD: %s seconds\n', st);
fprintf('The Root Mean Square ERROR using CPD: %2.4f pixel\n', cpd_rmse);


% movingReg use for NDT and ICP only:
movingReg = pctransform(movingProcessed, tform);

% Visualize alignment in top-view before and after registration
displayAlign = figure;

subplot(121)
pcshowpair(movingProcessed, fixedProcessed)
title('Before Registration')
view(2)

subplot(122)
pcshowpair(movingReg, fixedProcessed)
title('After Registration')
view(2)

displayAlign.HandleVisibility = 'callback';


% Improve the alignment after registration with pcmerge
ptCloudAccum = pcmerge(fixedProcessed, movingReg, mergeSize);

displayAccum = figure;
pcshow(ptCloudAccum)
title('EECE5554: Dense Map from LiDAR mapping')
view(2)

displayAccum.HandleVisibility = 'callback';

rng('default');
% Create a map builder object
mapBuilder = helperLidarMapBuilder('DownsamplePercent', downsampleSize);

% Set displya properties
closeDisplay = false;
totalFrames  = height(measurementLiDAR);

tform = rigid3d;
for n = 1 : skipFrames : totalFrames - skipFrames
    
    % Get the correct point cloud
    ptCloud = measurementLiDAR.PointCloud(n);
    
    % Use transformation from previous iteration as initial estimate for
    % current iteration of point cloud registration.
    initTform = tform;
    
    % Update map using the point cloud
    tform = updateMap(mapBuilder, ptCloud, initTform);
    
    % Update map display
    updateDisplay(mapBuilder, closeDisplay);
end


% Use the recorded GPS readings as a ground truth trajectory, to visually
% evaluate the quality of the dense map.
origin = [measurementGPS.Latitude(1), measurementGPS.Longitude(1), ...
    measurementGPS.Altitude(1)];

% Convert GPS readings to the local coordinate system
[xEast, yNorth, zTop] = latlon2local(measurementGPS.Latitude, ...
    measurementGPS.Longitude, measurementGPS.Altitude, origin);

% Estimate rough orientation at start of trajectory to align local
% coordinate system (LCS) with the lidar coordinate system
theta = median(atan2d(yNorth(1:15), xEast(1:15)));

R_LCS = [cosd(90-theta), sind(90-theta), 0;
        -sind(90-theta), cosd(90-theta), 0;
        0,               0,              1];

% Rotate the local coordinates to align with lidar coordinate system
groundTruthTrajectory = [xEast, yNorth, zTop] * R_LCS;


% Combine the ground truth gps data on the dense map.
hold(mapBuilder.Axes, 'on')
scatter(mapBuilder.Axes, groundTruthTrajectory(:,1), ...
    groundTruthTrajectory(:,2), 'red','filled');

displayLegen = legend(mapBuilder.Axes, {'LiDAR Points', ...
    'Est. no IMU', 'GPS Data'});
displayLegen.TextColor  = [1 1 1];
displayLegen.FontWeight = 'bold';

% Close map display
updateDisplay(mapBuilder, true);
reset(mapBuilder);

rng('default');

% Use IMU data to improve the dense map
initTform = rigid3d;
for n = 1 : skipFrames : totalFrames - skipFrames
    
    % Get the correct point cloud data
    ptCloud = measurementLiDAR.PointCloud(n);
    
    if n > 1
        % use the  imu data since the last lidar measurement.
        prevMeasurement = measurementLiDAR.Time(n - skipFrames);
        currMeasurement = measurementLiDAR.Time(n);
        measurementofLast = timerange(prevMeasurement, currMeasurement);
        
        imuMeasurement = measurementIMU(measurementofLast, 'Orientation');
        
        % Form an initial estimate using IMU measurement
        initTform = tform;

        % Compute the orientation change between the first and last 
        % reported IMU orientations during the last scan.
        q1_transform = imuMeasurement.Orientation(1);
        q2_transform = imuMeasurement.Orientation(end);

        % Compute rotational offset between first and last IMU measurement
        q_transform = q1_transform * conj(q2_transform);

        % Convert to Euler angles
        yawPitchRoll = euler(q_transform, 'ZYX', 'point');

        % Discard pitch and roll angle estimates. 
        yawPitchRoll(2:3) = 0;

        % Convert back to rotation matrix
        q_transform = quaternion(yawPitchRoll, 'euler', 'ZYX', 'point');
        R_transform = rotmat(q_transform, 'point');

        % Use computed rotation
        initTform.T(1:3, 1:3) = R_transform';
    end
    
    % Update map using the point cloud
    tform = updateMap(mapBuilder, ptCloud, initTform);
    
    % Update map display
    updateDisplay(mapBuilder, closeDisplay);
end


% Combine the ground truth gps data onto the dense map
hold(mapBuilder.Axes, 'on')
scatter(mapBuilder.Axes, groundTruthTrajectory(:,1), ...
    groundTruthTrajectory(:,2), 'red','filled');

displayLegen = legend(mapBuilder.Axes, {'LiDAR Points', ...
    'Est. w/ IMU', 'GPS Data'});
displayLegen.TextColor  = [1 1 1];
displayLegen.FontWeight = 'bold';


% Close open figures
close([displayFixed, displayAlign, displayAccum]);
updateDisplay(mapBuilder, true);

test=1;
