#!/usr/bin/env python

import rospy
import serial
import utm
from std_msgs.msg import String
from lab1.msg import GPS

def talker():
    pub = rospy.Publisher('gps_data', GPS, queue_size=10)
    rospy.init_node('gps_driver', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

def gps_sensor():
    # SENSOR_NAME = "gps"
    pub = rospy.Publisher('gps_data', GPS, queue_size=10)
    rospy.init_node('gps')
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baudrate',4800)
    sampling_rate = rospy.get_param('~sampling_rate',5.0)
    message = GPS()
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Using GPS sensor on port "+serial_port+" at "+str(serial_baud))
    rospy.logdebug("Initializing sensor with *0100P4\\r\\n ...")
    
    sampling_count = int(round(1/(sampling_rate*0.007913)))
    port.write('*0100EW*0100PR='+str(sampling_count)+'\r\n')  # cmd from 01 to 00 to set sampling period
    rospy.sleep(0.2)        
    line = port.readline()
    port.write('*0100P4\r\n')  # cmd from 01 to 00 to sample continuously
    
    sleep_time = 1/sampling_rate - 0.025
    
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            if line == '':
                rospy.logwarn("GPS: No data")
            else:
                if '$GPGGA' in line:
                    rospy.loginfo(line)
                    gpgga_data = line[:-4].split(',')
                    print(gpgga_data[2], gpgga_data[4])
                    print('Time: ' + gpgga_data[1] + ' | Latitude: ' + gpgga_data[2] + ' | Longitutde: ' + gpgga_data[4] + ' | Fix Quality: ' + gpgga_data[6])
                    print('Number of Satellites: ' + gpgga_data[7] + ' | HDOP: ' + gpgga_data[8] + ' | Altitude: ' + gpgga_data[9] + ' | Height Above Sea Level: ' + gpgga_data[11])
                    if gpgga_data[2] != '':
                        message.header.stamp = rospy.Time.now()
                        message.header.frame_id = "GPS Data"
                        lat = gpgga_data[2].replace('.', '')
                        lon = gpgga_data[4].replace('.', '')
                        message.Latitude = float(lat[:-6]) + round(float(lat[-6:])/600000, 5)
                        if gpgga_data[3] == 'S':
                            message.Latitude = -message.Latitude
                        message.Longitude = float(lon[:-6]) + round(float(lon[-6:])/600000, 5)
                        if gpgga_data[5] == 'W':
                            message.Longitude = -message.Longitude
                        try:
                            message.utm_easting, message.utm_northing, message.Zone, message.Letter = utm.from_latlon(message.Latitude, message.Longitude)
                        except:
                            pass
                        pub.publish(message)
            rospy.sleep(sleep_time)

            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down gps_sensor node...")

if __name__ == '__main__':
    try:
        gps_sensor()
    except rospy.ROSInterruptException:
        pass
