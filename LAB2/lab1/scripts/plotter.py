#!/usr/bin/env python 
import numpy as np
from matplotlib import pyplot as plt
import rospy
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion
import math

def plot_x(msg):
    global counter
    # if counter % 10 == 0:
    stamp = msg.header.stamp
    time = stamp.secs + stamp.nsecs * 1e-9
    r, p, y = euler_from_quaternion((msg.orientation.w, msg.orientation.x, msg.orientation.y, msg.orientation.z))
    print('Roll: {}'.format(math.degrees(r)))
    print('Pitch: {}'.format(math.degrees(p)))
    print('Yaw: {}'.format(math.degrees(y)))
    plt.plot(time, math.degrees(y), '*')
    plt.plot(time, math.degrees(p), 'o')
    plt.axis("equal")
    plt.draw()
    plt.pause(0.00000000001)

    counter += 1

if __name__ == '__main__':
    counter = 0

    rospy.init_node("plotter")
    rospy.Subscriber("imu_data", Imu, plot_x)
    plt.ion()
    plt.show()
    rospy.spin()