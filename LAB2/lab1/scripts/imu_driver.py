#!/usr/bin/env python

import rospy
import serial
import utm
from sensor_msgs.msg import Imu, MagneticField
from tf.transformations import quaternion_from_euler
import math

def talker():
    pub = rospy.Publisher('imu_data', IMU, queue_size=10)
    rospy.init_node('imu_driver', anonymous=True)
    rate = rospy.Rate(100) 
    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

def imu_sensor():
    # SENSOR_NAME = "imu"
    pub = rospy.Publisher('imu_data', Imu, queue_size=10)
    rospy.init_node('imu')
    serial_port = rospy.get_param('~port','/dev/ttyUSB1')
    serial_baud = rospy.get_param('~baudrate',115200)
    sampling_rate = rospy.get_param('~sampling_rate',5.0)
    message = Imu()
    pub_mag = rospy.Publisher('magnetometer_data', MagneticField, queue_size=10)
    message_magnet = MagneticField()
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Using IMU sensor on port "+serial_port+" at "+str(serial_baud))
    rospy.logdebug("Initializing sensor with *0100P4\\r\\n ...")
    
    sampling_count = int(round(1/(sampling_rate*0.007913)))
    port.write('*0100EW*0100PR='+str(sampling_count)+'\r\n')  # cmd from 01 to 00 to set sampling period
    rospy.sleep(0.2)        
    line = port.readline()
    port.write('*0100P4\r\n')  # cmd from 01 to 00 to sample continuously
    
    # sleep_time = 1/sampling_rate - 0.125
    
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            if line == '':
                rospy.logwarn("IMU: No data")
            else:
                if '$VNYMR' in line:
                    # vnymr = line[:-4].split(',')
                    # if vnymr[2] != '':
                    vnymr = line.split(',')
                    print(line)
                    rospy.loginfo(line)
                    message.header.stamp = rospy.Time.now()
                    message.header.frame_id = "IMU Data"
                    quaternion = quaternion_from_euler(math.radians(float(vnymr[3])), math.radians(float(vnymr[2])), math.radians(float(vnymr[1])))
                    message.orientation.x, message.orientation.y, message.orientation.z, message.orientation.w = [float(a) for a in quaternion]
                    message.angular_velocity.x, message.angular_velocity.y, message.angular_velocity.z = [float(a.split('*')[0]) for a in vnymr[10:]]
                    message.linear_acceleration.x, message.linear_acceleration.y, message.linear_acceleration.z = [float(a) for a in vnymr[7:10]]
                    pub.publish(message)
                    message_magnet.header.stamp = rospy.Time.now()
                    message_magnet.header.frame_id = "Magnetometer Data"
                    message_magnet.magnetic_field.x, message_magnet.magnetic_field.y, message_magnet.magnetic_field.z = [float(a) for a in vnymr[4:7]]
                    pub_mag.publish(message_magnet)
            # rospy.sleep(sleep_time)
            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down imu_sensor node...")

if __name__ == '__main__':
    try:
        imu_sensor()
    except rospy.ROSInterruptException:
        pass
