imu = table2array(imu_data);
mag = table2array(mag_data);
gps = table2array(gps_data);

% plot(orig_time(1:6600),imu_linear_acc_x(1:6600, 1))
% figure
% plot(orig_time, imu_linear_acc_x)
% figure;
% plot(imu(12500:68000, 1), rpy_arr(12500:68000, 3))
% hold on
% plot(imu(12500:68000, 1), rpy_arr(12500:68000, 2))
% plot(imu(12500:68000, 1), rpy_arr(12500:68000, 1))
% legend({'Roll', 'Pitch', 'Yaw'})
% hold off

orig_time = imu(:, 1)./10^9;

imu_linear_acc_x = imu(:, 9);
imu_linear_acc_y = imu(:, 10);
imu_linear_acc_z = imu(:, 11);

forw_vel = cumtrapz(orig_time(14500:68000), imu_linear_acc_x(14500:68000));

figure;
plot(orig_time(14500:68000), forw_vel)
xlabel('Time')
ylabel('Forward Velocity')
title('Forward Velocity of the IMU')


figure;
plot(gps(350:1700, 1)./10^9, utmVel(350:1700, 1))
xlabel('Time')
ylabel('Velocity')
title('GPS Velocity Estimate')

mean_fwd_vel = mean(imu_linear_acc_x(1:4600));
imu_linear_acc_x = imu_linear_acc_x - mean_fwd_vel;
% for c = 1:length(imu_linear_acc_x)
%     if imu_linear_acc_x(c) <= 0.23 && imu_linear_acc_x(c) >= -0.23
%         imu_linear_acc_x(c) = 0;
%     end
% end
    
forw_vel = cumtrapz(orig_time(14500:68000), imu_linear_acc_x(14500:68000));

figure;
plot(orig_time(14500:68000), forw_vel)
xlabel('Time')
ylabel('Forward Velocity')
title('Forward Velocity of the IMU (Offset Removed)')

dT = diff(gps(:, 1)*10^(-9));
utmDiff = diff(gps(:, 2:3));
utmVel = sqrt(utmDiff(:, 1).^2 + utmDiff(:, 2).^2)./dT;
figure;
plot(gps(350:1700, 1)./10^9, utmVel(350:1700, 1))
hold on
plot(orig_time(14500:68000), forw_vel)
xlabel('Time')
ylabel('Forward Velocity')
title('Forward Velocity of GPS vs IMU')
legend({'GPS', 'IMU'})
hold off
% figure;
% plot(orig_time, imu_linear_acc_x)
