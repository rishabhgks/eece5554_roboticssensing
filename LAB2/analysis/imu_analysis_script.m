% Loading the data
stat_imu = table2array(statimu);
time = stat_imu(:, 1);

% Dealing with Angular Velocity Data
angu_velo_x = stat_imu(:, 6);
angu_velo_y = stat_imu(:, 7);
angu_velo_z = stat_imu(:, 8);
figure;
subplot(3, 1, 1);
plot(time, angu_velo_x)
title('Plot of the Angular Velocity X axis')
xlabel('Time')
ylabel('Angular Velocity X Axis')
subplot(3, 1, 2);
plot(time, angu_velo_y)
title('Plot of the Angular Velocity Y axis')
xlabel('Time')
ylabel('Angular Velocity Y Axis')
subplot(3, 1, 3);
plot(time, angu_velo_z)
title('Plot of the Angular Velocity Z axis')
ylabel('Time')
xlabel('Angular Velocity Z Axis')
% legend({'X-axes Anguglar Velocity', 'Y-axes Angular Velocity', 'Z-axes Angular Velocity'})
figure;
hist(angu_velo_x, 100)
title('Plot of the Angular Velocity X axis')
ylabel('Frequency')
xlabel('Angular Velocity X Axis')
figure;
hist(angu_velo_y, 100)
title('Plot of the Angular Velocity Y axis')
ylabel('Frequency')
xlabel('Angular Velocity Y Axis')
figure;
hist(angu_velo_z, 100)
title('Plot of the Angular Velocity Z axis frequency distribution')
ylabel('Frequency')
xlabel('Angular Velocity Z Axis')
% Dealing with Linear Acceleration Data
line_acc_x = stat_imu(:, 8);
line_acc_y = stat_imu(:, 10);
line_acc_z = stat_imu(:, 11);
figure;
subplot(3, 1, 1);
plot(time, line_acc_x)
title('Plot of the Linear Acceleration X axis')
xlabel('Time')
ylabel('Linear Acceleration X Axis')
subplot(3, 1, 2);
plot(time, line_acc_y)
title('Plot of the Linear Acceleration Y axis')
xlabel('Time')
ylabel('Linear Acceleration Y Axis')
subplot(3, 1, 3);
plot(time, line_acc_z)
title('Plot of the Linear Acceleration Z axis')
xlabel('Time')
ylabel('Linear Acceleration Z Axis')
figure;
hist(line_acc_x, 100)
title('Plot of the Linear Acceleration X axis')
ylabel('Frequency')
xlabel('Linear Acceleration X Axis')
figure;
hist(line_acc_y, 100)
title('Plot of the Linear Acceleration Y axis')
ylabel('Frequency')
xlabel('Linear Acceleration Y Axis')
figure;
hist(line_acc_z, 100)
title('Plot of the Linear Acceleration Z axis frequency distribution')
ylabel('Frequency')
xlabel('Linear Acceleration Z Axis')

% Reading Magnetometer Data
stat_mag = table2array(statmag2);
time_mag = stat_mag(:, 1);
magn_x = stat_mag(:, 2);
magn_y = stat_mag(:, 3);
magn_z = stat_mag(:, 4);
figure;
subplot(3, 1, 1);
plot(time_mag, magn_x)
title('Plot of the Magnetometer X axis')
xlabel('Time')
ylabel('Magnetometer X Axis')
subplot(3, 1, 2);
plot(time_mag, magn_y)
title('Plot of the Magnetometer Y axis')
xlabel('Time')
ylabel('Magnetometer Y Axis')
subplot(3, 1, 3);
plot(time_mag, magn_z)
title('Plot of the Magnetometer Z axis')
xlabel('Time')
ylabel('Magnetometer Z Axis')
figure;
hist(magn_x, 20)
title('Plot of the Magnetometer X axis')
ylabel('Frequency')
xlabel('Magnetometer X Axis')
figure;
hist(magn_y, 20)
title('Plot of the Magnetometer Y axis')
ylabel('Frequency')
xlabel('Magnetometer Y Axis')
figure;
hist(magn_z, 20)
title('Plot of the Magnetometer Z axis frequency distribution')
ylabel('Frequency')
xlabel('Magnetometer Z Axis')


figure;
plot(time, orientation_x)
hold on
plot(time, orientation_y)
plot(time, orientation_z)
plot(time, orientation_w)
legend({'x', 'y', 'z', 'w'})
hold off

figure;
plot(time, line_acc_x)
hold on
plot(time, line_acc_y)
plot(time, line_acc_z)
legend({'x', 'y', 'z'})
hold off

figure;
plot(time, angu_velo_x)
hold on
plot(time, angu_velo_y)
plot(time, angu_velo_z)
legend({'x', 'y', 'z'})
hold off

figure;
plot(time_mag, magn_x)
hold on
plot(time_mag, magn_y)
plot(time_mag, magn_z)
hold off