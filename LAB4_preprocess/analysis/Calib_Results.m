% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 2304.926130542648025 ; 2311.170952136965298 ];

%-- Principal point:
cc = [ 1509.311007840448610 ; 1164.541185567560660 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.010451855808093 ; -0.104789401645819 ; 0.000062133947417 ; -0.001715116282241 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 9.116705187740168 ; 10.329732030774689 ];

%-- Principal point uncertainty:
cc_error = [ 11.445248566904308 ; 9.354118247440354 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.007611577970119 ; 0.023467004901143 ; 0.001505946080581 ; 0.001275624621765 ; 0.000000000000000 ];

%-- Image size:
nx = 2996;
ny = 2247;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 30;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 2.240561e+00 ; 2.181039e+00 ; -2.704831e-01 ];
Tc_1  = [ -4.585881e+02 ; -5.239702e+02 ; 1.547787e+03 ];
omc_error_1 = [ 2.763566e-03 ; 3.774963e-03 ; 6.839695e-03 ];
Tc_error_1  = [ 7.541819e+00 ; 6.395141e+00 ; 7.232901e+00 ];

%-- Image #2:
omc_2 = [ NaN ; NaN ; NaN ];
Tc_2  = [ NaN ; NaN ; NaN ];
omc_error_2 = [ NaN ; NaN ; NaN ];
Tc_error_2  = [ NaN ; NaN ; NaN ];

%-- Image #3:
omc_3 = [ NaN ; NaN ; NaN ];
Tc_3  = [ NaN ; NaN ; NaN ];
omc_error_3 = [ NaN ; NaN ; NaN ];
Tc_error_3  = [ NaN ; NaN ; NaN ];

%-- Image #4:
omc_4 = [ -2.100018e+00 ; -1.956426e+00 ; 7.941673e-01 ];
Tc_4  = [ -7.719682e+02 ; -6.613174e+02 ; 2.123299e+03 ];
omc_error_4 = [ 4.849361e-03 ; 1.992875e-03 ; 6.785045e-03 ];
Tc_error_4  = [ 1.032708e+01 ; 9.067486e+00 ; 9.624949e+00 ];

%-- Image #5:
omc_5 = [ -1.931807e+00 ; -1.913522e+00 ; 6.002477e-01 ];
Tc_5  = [ -8.110239e+02 ; -4.416119e+02 ; 2.126993e+03 ];
omc_error_5 = [ 4.533511e-03 ; 2.689565e-03 ; 6.058348e-03 ];
Tc_error_5  = [ 1.013168e+01 ; 9.023891e+00 ; 9.824894e+00 ];

%-- Image #6:
omc_6 = [ -1.655773e+00 ; -1.953264e+00 ; 2.614431e-01 ];
Tc_6  = [ -7.601607e+02 ; -3.195145e+02 ; 2.077667e+03 ];
omc_error_6 = [ 4.050340e-03 ; 3.687910e-03 ; 5.393201e-03 ];
Tc_error_6  = [ 9.899236e+00 ; 8.718647e+00 ; 9.731849e+00 ];

%-- Image #7:
omc_7 = [ NaN ; NaN ; NaN ];
Tc_7  = [ NaN ; NaN ; NaN ];
omc_error_7 = [ NaN ; NaN ; NaN ];
Tc_error_7  = [ NaN ; NaN ; NaN ];

%-- Image #8:
omc_8 = [ -1.537999e+00 ; -1.862558e+00 ; -1.446169e-01 ];
Tc_8  = [ -1.085724e+03 ; -3.300866e+02 ; 1.964010e+03 ];
omc_error_8 = [ 4.227704e-03 ; 4.612670e-03 ; 4.958844e-03 ];
Tc_error_8  = [ 9.749663e+00 ; 8.600292e+00 ; 9.585415e+00 ];

%-- Image #9:
omc_9 = [ NaN ; NaN ; NaN ];
Tc_9  = [ NaN ; NaN ; NaN ];
omc_error_9 = [ NaN ; NaN ; NaN ];
Tc_error_9  = [ NaN ; NaN ; NaN ];

%-- Image #10:
omc_10 = [ -1.696835e+00 ; -1.637306e+00 ; 9.045806e-01 ];
Tc_10  = [ -4.753842e+02 ; -3.677753e+02 ; 2.091723e+03 ];
omc_error_10 = [ 4.772849e-03 ; 3.227758e-03 ; 5.033101e-03 ];
Tc_error_10  = [ 1.015110e+01 ; 8.604634e+00 ; 8.796347e+00 ];

%-- Image #11:
omc_11 = [ -1.994550e+00 ; -1.913264e+00 ; 4.689043e-01 ];
Tc_11  = [ -8.573664e+02 ; -4.935550e+02 ; 2.522442e+03 ];
omc_error_11 = [ 4.529037e-03 ; 2.734911e-03 ; 6.318904e-03 ];
Tc_error_11  = [ 1.203806e+01 ; 1.057235e+01 ; 1.175241e+01 ];

%-- Image #12:
omc_12 = [ -1.347863e+00 ; -2.127445e+00 ; 1.615647e-01 ];
Tc_12  = [ -5.558930e+02 ; -6.555277e+02 ; 2.313417e+03 ];
omc_error_12 = [ 3.667786e-03 ; 4.019658e-03 ; 5.514365e-03 ];
Tc_error_12  = [ 1.124576e+01 ; 9.490780e+00 ; 1.093057e+01 ];

%-- Image #13:
omc_13 = [ -1.549181e+00 ; -1.994981e+00 ; -2.657359e-01 ];
Tc_13  = [ -9.396856e+02 ; -2.289412e+02 ; 2.056096e+03 ];
omc_error_13 = [ 3.905062e-03 ; 4.793447e-03 ; 5.133774e-03 ];
Tc_error_13  = [ 1.021407e+01 ; 8.789700e+00 ; 9.407001e+00 ];

%-- Image #14:
omc_14 = [ -1.621721e+00 ; -2.017371e+00 ; 9.804873e-02 ];
Tc_14  = [ -7.927415e+02 ; -1.734078e+02 ; 2.187308e+03 ];
omc_error_14 = [ 3.884207e-03 ; 4.042406e-03 ; 5.530282e-03 ];
Tc_error_14  = [ 1.048305e+01 ; 9.188890e+00 ; 1.017755e+01 ];

%-- Image #15:
omc_15 = [ -1.815650e+00 ; -1.954630e+00 ; 3.805195e-01 ];
Tc_15  = [ -7.323994e+02 ; -3.317119e+02 ; 2.112403e+03 ];
omc_error_15 = [ 4.169428e-03 ; 3.193991e-03 ; 5.766074e-03 ];
Tc_error_15  = [ 1.004237e+01 ; 8.845405e+00 ; 9.819077e+00 ];

%-- Image #16:
omc_16 = [ 2.351744e+00 ; 1.450625e+00 ; -1.146037e+00 ];
Tc_16  = [ -6.876734e+02 ; -6.036366e+02 ; 2.325498e+03 ];
omc_error_16 = [ 3.624493e-03 ; 4.738998e-03 ; 6.464784e-03 ];
Tc_error_16  = [ 1.144350e+01 ; 9.769053e+00 ; 9.788218e+00 ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ 2.798980e+00 ; 8.892731e-01 ; -1.062953e+00 ];
Tc_19  = [ -7.823652e+02 ; 6.609885e+01 ; 2.332478e+03 ];
omc_error_19 = [ 3.726904e-03 ; 3.919641e-03 ; 6.764893e-03 ];
Tc_error_19  = [ 1.114436e+01 ; 9.725320e+00 ; 1.031604e+01 ];

%-- Image #20:
omc_20 = [ -2.105558e+00 ; -1.929097e+00 ; 5.710448e-01 ];
Tc_20  = [ -5.702416e+02 ; -5.882860e+02 ; 2.065690e+03 ];
omc_error_20 = [ 4.355166e-03 ; 2.180902e-03 ; 6.367036e-03 ];
Tc_error_20  = [ 9.974067e+00 ; 8.557403e+00 ; 9.501126e+00 ];

%-- Image #21:
omc_21 = [ NaN ; NaN ; NaN ];
Tc_21  = [ NaN ; NaN ; NaN ];
omc_error_21 = [ NaN ; NaN ; NaN ];
Tc_error_21  = [ NaN ; NaN ; NaN ];

%-- Image #22:
omc_22 = [ -2.252988e+00 ; -2.083836e+00 ; 4.264164e-01 ];
Tc_22  = [ -4.826096e+02 ; -2.880283e+02 ; 1.904288e+03 ];
omc_error_22 = [ 3.724253e-03 ; 2.659861e-03 ; 7.264103e-03 ];
Tc_error_22  = [ 9.141889e+00 ; 7.825960e+00 ; 8.614841e+00 ];

%-- Image #23:
omc_23 = [ -2.221481e+00 ; -2.175876e+00 ; 3.591072e-01 ];
Tc_23  = [ -8.616224e+02 ; -3.460616e+02 ; 1.797840e+03 ];
omc_error_23 = [ 4.247971e-03 ; 2.451913e-03 ; 7.668789e-03 ];
Tc_error_23  = [ 8.555598e+00 ; 7.809699e+00 ; 8.492220e+00 ];

%-- Image #24:
omc_24 = [ -2.074923e+00 ; -1.973149e+00 ; 8.501805e-02 ];
Tc_24  = [ -4.627660e+02 ; -4.248500e+02 ; 1.619971e+03 ];
omc_error_24 = [ 3.772545e-03 ; 3.049160e-03 ; 6.109954e-03 ];
Tc_error_24  = [ 7.852917e+00 ; 6.643791e+00 ; 7.373274e+00 ];

%-- Image #25:
omc_25 = [ 2.025699e+00 ; 1.770529e+00 ; -9.369592e-01 ];
Tc_25  = [ -3.874124e+02 ; -3.151577e+02 ; 1.936193e+03 ];
omc_error_25 = [ 2.930673e-03 ; 4.127519e-03 ; 6.458448e-03 ];
Tc_error_25  = [ 9.545510e+00 ; 7.869430e+00 ; 7.387635e+00 ];

%-- Image #26:
omc_26 = [ NaN ; NaN ; NaN ];
Tc_26  = [ NaN ; NaN ; NaN ];
omc_error_26 = [ NaN ; NaN ; NaN ];
Tc_error_26  = [ NaN ; NaN ; NaN ];

%-- Image #27:
omc_27 = [ -1.076143e+00 ; -2.266561e+00 ; 1.504984e-01 ];
Tc_27  = [ -1.877549e+02 ; -3.896410e+02 ; 1.433673e+03 ];
omc_error_27 = [ 2.760428e-03 ; 4.065939e-03 ; 5.200507e-03 ];
Tc_error_27  = [ 6.979180e+00 ; 5.845750e+00 ; 6.711839e+00 ];

%-- Image #28:
omc_28 = [ -1.841618e+00 ; -1.521569e+00 ; 8.970886e-01 ];
Tc_28  = [ -2.760306e+02 ; -3.843296e+02 ; 2.378466e+03 ];
omc_error_28 = [ 4.604266e-03 ; 2.813843e-03 ; 5.150212e-03 ];
Tc_error_28  = [ 1.166548e+01 ; 9.636581e+00 ; 9.787796e+00 ];

%-- Image #29:
omc_29 = [ -1.760370e+00 ; -1.628431e+00 ; 8.952081e-01 ];
Tc_29  = [ -3.236696e+02 ; -4.129873e+02 ; 2.330768e+03 ];
omc_error_29 = [ 4.575918e-03 ; 2.916600e-03 ; 5.172067e-03 ];
Tc_error_29  = [ 1.140941e+01 ; 9.468718e+00 ; 9.656823e+00 ];

%-- Image #30:
omc_30 = [ NaN ; NaN ; NaN ];
Tc_30  = [ NaN ; NaN ; NaN ];
omc_error_30 = [ NaN ; NaN ; NaN ];
Tc_error_30  = [ NaN ; NaN ; NaN ];

