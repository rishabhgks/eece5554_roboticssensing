load('EECE5554_lab3.mat');
format long;
%-------------------------------------------%
% Stationary Data outside in an Open field %
%-------------------------------------------%

%Unique values of easting and northing
unique_easting = unique(rtkroverstatopengpsdata.fieldutm_easting)
unique_northing = unique(rtkroverstatopengpsdata.fieldutm_northing)

%Scatter plot of easting vs northing
scatter(rtkroverstatopengpsdata.fieldutm_easting, rtkroverstatopengpsdata.fieldutm_northing)
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Stationary data scatter plot of Easting vs Northing from Open area");

figure;
hist(rtkroverstatopengpsdata.fieldutm_easting, 20);
xlabel("UTM Easting");
ylabel("Frequency");
title("Stationary data Histogram plot of UTM Easting from Open area");

figure;
hist(rtkroverstatopengpsdata.fieldutm_northing, 20);
xlabel("UTM Northing");
ylabel("Frequency");
title("Stationary data Histogram plot of UTM Northing from Open area");

rms_stat_easting_Open = rms(rtkroverstatopengpsdata.fieldutm_easting - mean(rtkroverstatopengpsdata.fieldutm_easting))
rms_stat_northing_Open = rms(rtkroverstatopengpsdata.fieldutm_northing - mean(rtkroverstatopengpsdata.fieldutm_northing))