load('EECE5554_lab3.mat');
format long;
%-------------------------------------------%
% Moving Data outside Open %
%-------------------------------------------%

%Scatter plot of easting vs northing
scatter(rtkrovermoveopengpsdata.fieldutm_easting(1:420), rtkrovermoveopengpsdata.fieldutm_northing(1:420))
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Moving data scatter plot of Easting vs Northing from Open area");

p1 = polyfit(rtkrovermoveopengpsdata.fieldutm_easting(1:130), rtkrovermoveopengpsdata.fieldutm_northing(1:130), 1)
poly1 = polyval(p1, rtkrovermoveopengpsdata.fieldutm_easting(1:130))
p2 = polyfit(rtkrovermoveopengpsdata.fieldutm_easting(145:230), rtkrovermoveopengpsdata.fieldutm_northing(145:230), 1)
poly2 = polyval(p2, rtkrovermoveopengpsdata.fieldutm_easting(145:230))
p3 = polyfit(rtkrovermoveopengpsdata.fieldutm_easting(240:330), rtkrovermoveopengpsdata.fieldutm_northing(240:330), 1)
poly3 = polyval(p3, rtkrovermoveopengpsdata.fieldutm_easting(240:330))
p4 = polyfit(rtkrovermoveopengpsdata.fieldutm_easting(340:420), rtkrovermoveopengpsdata.fieldutm_northing(340:420), 1)
poly4 = polyval(p4, rtkrovermoveopengpsdata.fieldutm_easting(340:420))

scatter(rtkrovermoveopengpsdata.fieldutm_easting(1:130), rtkrovermoveopengpsdata.fieldutm_northing(1:130))
hold on
plot(rtkrovermoveopengpsdata.fieldutm_easting(1:130), poly1)
scatter(rtkrovermoveopengpsdata.fieldutm_easting(145:230), rtkrovermoveopengpsdata.fieldutm_northing(145:230))
plot(rtkrovermoveopengpsdata.fieldutm_easting(145:230), poly2)
scatter(rtkrovermoveopengpsdata.fieldutm_easting(240:330), rtkrovermoveopengpsdata.fieldutm_northing(240:330))
plot(rtkrovermoveopengpsdata.fieldutm_easting(240:330), poly3)
scatter(rtkrovermoveopengpsdata.fieldutm_easting(340:420), rtkrovermoveopengpsdata.fieldutm_northing(340:420))
plot(rtkrovermoveopengpsdata.fieldutm_easting(340:420), poly4)
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Moving data scatter plot of Easting vs Northing from Open area with Line Fits");
hold off

rms_moving_northing_p1 = rms(rtkrovermoveopengpsdata.fieldutm_northing(1:130) - poly1)
rms_moving_northing_p2 = rms(rtkrovermoveopengpsdata.fieldutm_northing(145:230) - poly2)
rms_moving_northing_p3 = rms(rtkrovermoveopengpsdata.fieldutm_northing(240:330) - poly3)
rms_moving_northing_p4 = rms(rtkrovermoveopengpsdata.fieldutm_northing(340:420) - poly4)
rms_moving_northing = mean([rms_moving_northing_p1 rms_moving_northing_p2 rms_moving_northing_p3 rms_moving_northing_p4])

hist([(rtkrovermoveopengpsdata.fieldutm_northing(1:130) - poly1);(rtkrovermoveopengpsdata.fieldutm_northing(145:230) - poly2);(rtkrovermoveopengpsdata.fieldutm_northing(240:330) - poly3);(rtkrovermoveopengpsdata.fieldutm_northing(340:420) - poly4)])
xlabel("UTM Northing RMS error values");
ylabel("Frequency");
title("Moving data RMSE plot of Northing from Open area");