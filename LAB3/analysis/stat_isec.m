load('EECE5554_lab3.mat');
format long;
%-------------------------------------------%
% Stationary Data outside ISEC %
%-------------------------------------------%

%Unique values of easting and northing
unique(rtkroverstatisecgpsdata.fieldutm_easting);
unique(rtkroverstatisecgpsdata.fieldutm_northing);

%Scatter plot of easting vs northing
scatter(rtkroverstatisecgpsdata.fieldutm_easting, rtkroverstatisecgpsdata.fieldutm_northing)
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Stationary data scatter plot of Easting vs Northing from ISEC area");

figure;
hist(rtkroverstatisecgpsdata.fieldutm_easting, 20);
xlabel("UTM Easting");
ylabel("Frequency");
title("Stationary data Histogram plot of UTM Easting from ISEC area");

figure;
hist(rtkroverstatisecgpsdata.fieldutm_northing, 20);
xlabel("UTM Northing");
ylabel("Frequency");
title("Stationary data Histogram plot of UTM Northing from ISEC area");

rms_stat_easting_isec = rms(rtkroverstatisecgpsdata.fieldutm_easting - mean(rtkroverstatisecgpsdata.fieldutm_easting))
rms_stat_northing_isec = rms(rtkroverstatisecgpsdata.fieldutm_northing - mean(rtkroverstatisecgpsdata.fieldutm_northing))