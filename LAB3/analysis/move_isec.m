load('EECE5554_lab3.mat');
format long;
%-------------------------------------------%
% Moving Data outside ISEC %
%-------------------------------------------%

%Scatter plot of easting vs northing
scatter(rtkrovermove1isecgpsdata.fieldutm_easting(490:540), rtkrovermove1isecgpsdata.fieldutm_northing(490:540))
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Moving data scatter plot of Easting vs Northing from ISEC area");

p1 = polyfit(rtkrovermove1isecgpsdata.fieldutm_easting(1:185), rtkrovermove1isecgpsdata.fieldutm_northing(1:185), 1)
poly1 = polyval(p1, rtkrovermove1isecgpsdata.fieldutm_easting(1:185))
p2 = polyfit(rtkrovermove1isecgpsdata.fieldutm_easting(205:290), rtkrovermove1isecgpsdata.fieldutm_northing(205:290), 1)
poly2 = polyval(p2, rtkrovermove1isecgpsdata.fieldutm_easting(205:290))
p3 = polyfit(rtkrovermove1isecgpsdata.fieldutm_easting(300:475), rtkrovermove1isecgpsdata.fieldutm_northing(300:475), 1)
poly3 = polyval(p3, rtkrovermove1isecgpsdata.fieldutm_easting(300:475))
p4 = polyfit(rtkrovermove1isecgpsdata.fieldutm_easting(490:540), rtkrovermove1isecgpsdata.fieldutm_northing(490:540), 1)
poly4 = polyval(p4, rtkrovermove1isecgpsdata.fieldutm_easting(490:540))

scatter(rtkrovermove1isecgpsdata.fieldutm_easting(1:185), rtkrovermove1isecgpsdata.fieldutm_northing(1:185))
hold on
plot(rtkrovermove1isecgpsdata.fieldutm_easting(1:185), poly1)
scatter(rtkrovermove1isecgpsdata.fieldutm_easting(205:290), rtkrovermove1isecgpsdata.fieldutm_northing(205:290))
plot(rtkrovermove1isecgpsdata.fieldutm_easting(205:290), poly2)
scatter(rtkrovermove1isecgpsdata.fieldutm_easting(300:475), rtkrovermove1isecgpsdata.fieldutm_northing(300:475))
plot(rtkrovermove1isecgpsdata.fieldutm_easting(300:475), poly3)
scatter(rtkrovermove1isecgpsdata.fieldutm_easting(490:540), rtkrovermove1isecgpsdata.fieldutm_northing(490:540))
plot(rtkrovermove1isecgpsdata.fieldutm_easting(490:540), poly4)
xlabel("UTM Easting");
ylabel("UTM Northing");
title("Moving data scatter plot of Easting vs Northing from ISEC area with Line Fits");
hold off

rms_moving_northing_p1 = rms(rtkrovermove1isecgpsdata.fieldutm_northing(1:185) - poly1)
rms_moving_northing_p2 = rms(rtkrovermove1isecgpsdata.fieldutm_northing(205:290) - poly2)
rms_moving_northing_p3 = rms(rtkrovermove1isecgpsdata.fieldutm_northing(300:475) - poly3)
rms_moving_northing_p4 = rms(rtkrovermove1isecgpsdata.fieldutm_northing(490:540) - poly4)
rms_moving_northing = mean([rms_moving_northing_p1 rms_moving_northing_p2 rms_moving_northing_p3 rms_moving_northing_p4])

hist([(rtkrovermove1isecgpsdata.fieldutm_northing(1:185) - poly1);(rtkrovermove1isecgpsdata.fieldutm_northing(205:290) - poly2);(rtkrovermove1isecgpsdata.fieldutm_northing(300:475) - poly3);(rtkrovermove1isecgpsdata.fieldutm_northing(490:540) - poly4)])
xlabel("UTM Northing RMS error values");
ylabel("Frequency");
title("Moving data RMSE plot of Northing from ISEC area");